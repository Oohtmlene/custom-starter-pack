import React from 'react';
import Button from '../Button';
import { string, func } from 'prop-types';

const PrevNext = ({ ...props }) => {
  console.log('7- total steps', props.nextText, props.totalSteps, 'currentStep', props.currentStep)
  return (
    <ul className="prev-next">
      {props.currentStep > 0 &&
        <Button
          type="primary"
          text={props.prevText}
          onClick={props.handlePrev}
        />
      }
      {props.currentStep < props.totalSteps - 1 &&
        <Button
          type="primary"
          text={props.nextText}
          onClick={props.handleNext}
        />
      }
    </ul>
  );
};

PrevNext.propTypes = {
  prevText: string,
  nextText: string,
  handleNext: func,
  handlePrev: func,
};

PrevNext.defaultProps = {
  prevText: 'Previous step',
  nextText: 'Next step',
  handleNext: () => {},
  handlePrev: () => {},
};

export default PrevNext;