import React from 'react';
import { bool, func, number, string } from 'prop-types';

const Step = (props) => {
  console.log('6 step props', props);

  return (
    <div className={`step step${props.step}`}>
      { props.content }
    </div>
  );
};

Step.propTypes = {
};

Step.defaultProps = {
};

export default Step;