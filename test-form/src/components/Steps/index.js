import React, { Component } from 'react';
import { bool, func, number, string } from 'prop-types';
import Step from './components/Step';
import PrevNext from '../PrevNext';
import StepsBar from '../StepsBar';

class Steps extends Component {
  static propTypes = {
    total: number.isRequired,
  };

  static defaultProps = {
  };

  constructor() {
    super();
    this.state = {
      step: 0,
    }
  }

  handlePrev = (e) => {
    const valid = false;
    e.preventDefault();
    // Handle validation
    console.log(valid);
    this.setState({ step: this.state.step - 1 });
  }

  // Need to handle vaidation as well - where
  handleNext = (e) => {
    e.preventDefault();
    this.setState({ step: this.state.step + 1 });// Need to handle vaidation as well - where
  }

  render() {
    const stepArray = this.props.children;
    console.log('27 stepArray', stepArray);
    const stepsList = stepArray.map((step, i) => {
      return (
        <div>
          <Step key={i.toString} step={i + 1} content={step.props.children} />
        </div>
      );
    });
    return (
      <div>
        step bar here
        <StepsBar
          total={stepArray.length}
          step={this.state.step}
        />
        {stepsList[this.state.step]}
        <PrevNext
          currentStep={this.state.step}
          totalSteps={this.props.total}
          prevText={stepArray[this.state.step].props.prevText}
          nextText={stepArray[this.state.step].props.nextText}
          handlePrev={this.handlePrev}
          handleNext={this.handleNext}
        />
      </div>
    );
  }
}

export default Steps;
