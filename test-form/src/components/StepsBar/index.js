import React from 'react';
import { number, string } from 'prop-types';
import Step from './components/Step';

const StepsBar = ({ ...props }) => {
  console.log("6 stepbar props", props);
  return (
    <ul className={`steps steps--${props.className}`} >
      {[...Array(props.total)].map((n, i) =>
        (
          <Step
            key={i.toString()}
            num={i + 1}
            active={props.step === i}
            completed={props.step > i}
          />
        ))}
    </ul>
  );
};

StepsBar.propTypes = {
  total: number.isRequired,
  step: number.isRequired,
  className: string,
};

StepsBar.defaultProps = {
  total: 1,
  step: 0,
  className: 'default',
};

export default StepsBar;
