import React, { Component } from 'react';
import { bool, func, string } from 'prop-types';
import { inject, observer } from 'mobx-react';
import Aux from 'react-aux'; // self destructing component
import Text from './components/Text';
import Radio from './components/Radio';
import Checkbox from './components/Checkbox';


// functional tastless component
@inject('FormStore2')
@observer
class Input extends Component {

 
 

  //= how handle password, checkbox, radio
  render() {

    console.log('11- input props', this.props);
    return (
      <Aux>
        {this.props.type === 'text' &&
          <Text
            {...this.props}
            field
          />
        }
        {this.props.type === 'radio' &&
          <Radio
            {...this.props}
          />
        }
        {this.props.type === 'checkbox' &&
          <Checkbox
            {...this.props}
          />
        }
      </Aux>
    );
  }
}

Input.propTypes = {
  type: string,
  handleChange: func,
  handleBlur: func,
  field: bool,
};

Input.defaultProps = {
  type: 'text',
  handleChange: () => { },
  handleBlur: () => { },
  field: true,
};

export default Input;

