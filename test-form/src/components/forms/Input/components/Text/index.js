import React from 'react';
import { bool, func, string } from 'prop-types';

const Text = (props) => {
  const handleChange = (e) => {
    // update store with value
    props.handleChange(props.name, e.target.value);
  };
  const handleBlur = (e) => {
    props.handleBlur(props.name, e.target.value);
  };
  return (
    //= how handle password, checkbox, radio
    <input
      childType={props.childType}
      type={props.type}
      id={`rf-${props.name}`}
      name={props.name}
      autoComplete={props.autocomplete}
      placeholder="test"
      value={props.value}
      onChange={handleChange}
      onBlur={handleBlur}
    />
  );
};

Text.propTypes = {
  name: string.isRequired,
  type: string,
  autocomplete: bool,
  placeholder: string,
  value: string,
  handleChange: func,
  handleBlur: func,
  childType: string,
};

Text.defaultProps = {
  type: 'text',
  autocomplete: false,
  placeholder: 'placeholder',
  value: '',
  childType: 'field',
  handleChange: () => { },
  handleBlur: () => { },
};

export default Text;

