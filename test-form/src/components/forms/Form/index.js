import React, { Component } from 'react';
import {
  arrayOf,
  bool,
  func,
  objectOf,
  number,
  string,
  shape
} from 'prop-types';
import { inject, observer } from 'mobx-react';

@inject('FormStore2')
@observer
class Form extends React.Component {
  static propTypes = {
    action: string.isRequired,
    onSuccess: func.isRequired,
    handleFetch: func,
    
  }
  static defaultProps = {
    handleFetch: () => {},
  }

  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  //= Handle events
  handleChange = (name, value) => this.props.FormStore2.handleChange(name, value);

  handleBlur = (name, value) => this.props.FormStore2.handleBlur(name, value);
  //= in the store?
  handleSubmit = (e) => {
    e.preventDefault();
    const valid = this.props.FormStore2.isFormValid();
    // Check if all fields valid
    if (valid) {
      const postData = {};
      //= Collect fields from store (name/value pair)
      const Storefields = this.props.FormStore2.fields;
      //= Build the data array
      Object.keys(Storefields).forEach((name) => {
        postData[name] = Storefields[name].value;
      });

      // HANDLE FETCH
      // Specific fetch
      if (this.props.handleFetch.length) {
        this.props.handleFetch(postData);
        return false;
      }
      // Generic fetch
      fetch(this.props.action, {
        method: 'post',
        body: JSON.stringify(postData),
        credentials: 'include', // For cross-origin call
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
        // Check if response ok
        .then((response) => {
          const json = response.json();
          if (response.ok) {
            return json;
          }
          return json.then((err) => { throw err; });
        })
        // When server return 2xx
        .then((response) => {
          // local storage
          // this.state.fields.map(field =>
          //   this.storeData(field.name, field.value),
          // );
          this.props.onSuccess(response);
          // this.handleScroll();
          this.setState({
            loading: false,
          });
        })
        .catch((error) => { // When server does not return 2xx
          if (this.props.onError) {
            this.props.onError(error);
          } else {
            this.setState({
              globalError: 'Something has gone wrong on our side. Please try again later.',
            });
          }
          this.setState({
            loading: false,
            disabled: false,
            errorHint: this.props.errorHint,
          });
        });
      return true;
    }
  }

  render() {
    return (
      <form
        className={`form form--${this.props.className}`}
        action=""
        onSubmit={this.handleSubmit}
      > {this.state.loading && <span className="is-loading">Loading...</span>}
        <div className={'form__inner'}>
          {this.props.children}
        </div>
        <input type="submit" value="Submit" />
      </form>
    )
  };
}

export default Form;
