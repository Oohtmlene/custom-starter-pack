import React from 'react';
import { bool, func, number, string } from 'prop-types';
import FormElement from './components/FormElement';

const Field = (props) => {
  const className = props.type ? `form__field--${props.type}` : '';
  return (
    <div className={`form__field form__field--${props.formElement} ${className}`}>
      {props.label &&
        <label className="form__label">
          {props.label}
          {props.required === true &&
            <span className="required">*</span>
          }
        </label>
      }
      <FormElement {...props} />
    </div>
  );
};

Field.propTypes = {
  type: string,
  label: string,
  required: bool,
  formElement: string,
};

Field.defaultProps = {
  type: '',
  label: '',
  formElement: 'input',
  required: false,
};

export default Field;
