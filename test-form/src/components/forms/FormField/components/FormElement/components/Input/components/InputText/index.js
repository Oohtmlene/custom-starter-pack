import React from 'react';
import { bool, func, string } from 'prop-types';

const InputText = (props) => {
  const handleChange = (e) => {
    props.handleChange(props.name, e.target.value);
  };

  const handleBlur = (e) => {
    props.handleBlur(props.name, e.target.value);
  };

  return (
    //= how handle password, checkbox, radio
    <input
      type={props.type}
      id={`rf-${props.name}`}
      name={props.name}
      autoComplete={props.autocomplete}
      placeholder={props.placeholder}
      value={props.value}
      onChange={handleChange}
      onBlur={handleBlur}
    />
  );
};

InputText.propTypes = {
  name: string.isRequired,
  type: string,
  autocomplete: bool,
  placeholder: string,
  value: string,
  handleChange: func,
  handleBlur: func,
};

InputText.defaultProps = {
  type: 'text',
  autocomplete: false,
  placeholder: 'placeholder',
  value: '',
  handleChange: () => { },
  handleBlur: () => { },
};

export default InputText;

