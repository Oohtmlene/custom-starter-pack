import React from 'react';
import { bool, func, string } from 'prop-types';
import Aux from 'react-aux'; // self destructing component
import InputText from './components/InputText';
import InputRadio from './components/InputRadio';
import InputCheckbox from './components/InputCheckbox';
import InputCalendar from './components/InputCalendar';

// functional tastless component
const Input = (props) => {
  // const handleChange = (e) => {
  //   console.log('13 e', e)
  //   const name = props.name;
  //   const value = e.target.value;
  //   console.log('15 - name, value', name, value);
  //   props.handleChange(name, value);
  // };

  // const handleBlur = (e) => {
  //   const name = props.name;
  //   const value = e.target.value;
  //   props.handleBlur(name, value);
  // };

  //= how handle password, checkbox, radio
  return (
    <Aux>
      { props.type === 'text' &&
        <InputText
          {...props}
        />
      }
      { props.type === 'radio' &&
        <InputRadio
          {...props}
        />
      }
      { props.type === 'checkbox' &&
        <InputCheckbox
          {...props}
        />
      }
    </Aux>
  );
};

Input.propTypes = {
  type: string,
  handleChange: func,
  handleBlur: func,
};

Input.defaultProps = {
  type: 'text',
  handleChange: () => {},
  handleBlur: () => {},
};

export default Input;

