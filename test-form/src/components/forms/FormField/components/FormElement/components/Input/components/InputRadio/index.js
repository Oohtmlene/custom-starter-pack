import React from 'react';
import { arrayOf, bool, func, string } from 'prop-types';

// functional tastless component
const InputRadio = (props) => {

  const handleChange = (e) => {
    props.handleChange(props.name, e.target.value);
  };

  const handleBlur = (e) => {
    props.handleBlur(props.name, e.target.value);
  };

  const options = props.options.map(option => (
    <li className={`radio-list__item ${option.checked ? 'radio-list__item--checked' : ''}`}>
      <label htmlFor={option.name}>{option.value}</label>
      <input
        type={props.type}
        id={`rf-${option.name}`}
        name={props.name}
        value={option.value}
        onChange={handleChange}
        onBlur={handleBlur}
        checked={option.checked}
      />
    </li>
  ));

  //= how handle password, checkbox, radio
  return (
    <ul className="radio-list">
      {options}
    </ul>
  );
};

InputRadio.propTypes = {
  type: string,
  name: string.isRequired,
  options: arrayOf(string).isRequired,
  handleChange: func,
  handleBlur: func,
};

InputRadio.defaultProps = {
  type: 'radio',
  handleChange: () => { },
  handleBlur: () => { },
};

export default InputRadio;

