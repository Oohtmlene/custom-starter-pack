import React, { Component } from 'react';
import { bool, func, number, string } from 'prop-types';
import { inject, observer } from 'mobx-react';
import Input from './components/Input';
import Select from './components/Select';
import Textarea from './components/Textarea';
import './form-element.css';

@inject('FormStore')
@observer
//= should it be stateless? probably yes
class FormElement extends Component {
  static propTypes = { }

  static defaultProps = { }

  render() {
    // console.log('form Element props', this.props);
    return (
      <div className={`form__element ${this.props.FormStore.fields[this.props.name].error ? 'form__element--error' : ''}`}> 
          {this.props.formElement === 'input' &&
            <Input
              {...this.props}
            />
          }
          {this.props.formElement === 'select' &&
            <Select
              {...this.props}
            />
          }
          {this.props.formElement === 'textarea' &&
            <Textarea
              {...this.props}
            />
          }
          { this.props.FormStore.fields[this.props.name].error &&
             <span
              className={'form__error'}
            >
              {this.props.errorMessage}
           </span>
          }
      </div>
    )
  }
}

FormElement.propTypes = {

};

FormElement.defaultProps = {

};

export default FormElement;
