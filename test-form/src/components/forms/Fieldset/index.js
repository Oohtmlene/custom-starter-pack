import React from 'react';
import { } from 'prop-types';
import FormField from './components/FormField'

const Fieldset = (props) => {
  console.log('3 fieldset props', ...props);
  return (
    <fieldset className="form__fieldset">  { console.log('12 this.props', props) }
      <legend className="form__fieldset__legend">{props.legend}</legend> 
      {props.intro &&
        <p className="form__fieldset__intro">{props.intro}</p>
      }
      {props.fields.map(field => (
        <FormField
          {...field}
          key={field.id}
          handleChange={props.handleChange}
          handleBlur={props.handleBlur}
        />
      ))}
      {props.additionalInfo &&
        <p className="form__fieldset__additionalInfo">{props.additionalInfo}</p>
      }
    </fieldset>
  )
}

export default Fieldset;