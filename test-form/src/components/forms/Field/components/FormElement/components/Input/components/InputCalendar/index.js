import React from 'react';
import { bool, func, string } from 'prop-types';

// functional tastless component
const InputCalendar = (props) => {

  //= how handle password, checkbox, radio
  return (
      <input
        type={props.type}
        id={`rf-${props.name}`}
        name={props.name}
        autoComplete={props.autocomplete}
        autoFocus={props.autofocus}
        placeholder={props.placeholder}
        value={props.value}
        onChange={props.handleChange}
        onBlur={props.handleBlur}
      />
  );
}

InputCalendar.propTypes = {
};

InputCalendar.defaultProps = {
};

export default InputCalendar;

