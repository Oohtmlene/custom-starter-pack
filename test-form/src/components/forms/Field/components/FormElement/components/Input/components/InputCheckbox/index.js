import React from 'react';
import { bool, func, string } from 'prop-types';
import './input-checkbox.css';

// functional tastless component
const InputCheckbox = ((props) => {

  //= must return pair name, value ex rural: true
  const handleChange = (() => {
    const checked = !props.checked;
    console.log('15 - name, value', checked);
    props.handleChange(props.name, checked);
  });

  return (
    <input
      type={props.type}
      id={`rf-${props.name}`}
      name={props.name}
      checked={props.checked}
      value={props.value}
      onChange={handleChange}
      //onBlur={handleBlur}
    />
  )
});

InputCheckbox.propTypes = {
  type: string,
  name: string,
  checked: bool,
  value: string,
  handleChange: func,
  handleBlur: func,
};

InputCheckbox.defaultProps = {
  type: 'checkbox',
  name: '',
  checked: false,
  value: '',
  handleChange: () => { },
  handleBlur: () => { },
};

export default InputCheckbox;

