import React from 'react';
import { bool, func, string } from 'prop-types';
import Aux from 'react-aux'; // self destructing component
import InputText from './components/InputText';
import InputRadio from './components/InputRadio';
import InputCheckbox from './components/InputCheckbox';
import InputCalendar from './components/InputCalendar';


// functional tastless component
const Input = (props) => {
  //= how handle password, checkbox, radio
  return (
    <Aux>
      { (
          props.type === 'text' ||
          props.type === 'phone' ||
          props.type === 'email'
        )
        &&
        <InputText
          {...props}
        />
      }
      {props.type === 'radio' &&
        <InputRadio
          {...props}
        />
      }
      {props.type === 'checkbox' &&
        <InputCheckbox
          {...props}
        />
      }
    </Aux>
  );
};

Input.propTypes = {
  type: string,
  handleChange: func,
  handleBlur: func,
};

Input.defaultProps = {
  type: 'text',
  handleChange: () => { },
  handleBlur: () => { },
};

export default Input;

