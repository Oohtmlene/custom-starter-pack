import React from 'react';
import { arrayOf, bool, func, shape, string } from 'prop-types';

const Select = (props) => {

  const handleChange = (e) => {
    const name = props.name;
    const value = e.target.value;
    props.handleChange(name, value);
  }

  const handleBlur = (e) => {
    const name = props.name;
    const value = e.target.value;
    props.handleBlur(name, value);
  }

  return (
    <select
      // add attribute
      name={props.name}
      value={props.value}
      onChange={handleChange}
      onBlur={handleBlur}
    >
      <option value="select">Please select</option>
      {props.options.map(option =>
        <option
          key={option.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
          value={option.value}
        >
          {option.value}
        </option>
      )
      }
    </select>
  );
}

Select.propTypes = {
};

Select.defaultProps = {
};

export default Select;

