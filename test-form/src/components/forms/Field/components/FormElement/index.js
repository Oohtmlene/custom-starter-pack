import React, { Component } from 'react';
import { bool, func, number, string } from 'prop-types';
import { inject, observer } from 'mobx-react';
import Input from './components/Input';
import Select from './components/Select';
import Textarea from './components/Textarea';
import './form-element.css';

@inject('FormStore2')
@observer
//= should it be stateless? probably yes
class FormElement extends Component {
  static propTypes = {}

  static defaultProps = {}

  handleChange = (name, value) => this.props.FormStore2.handleChange(name, value);
  handleBlur = (name, value) => this.props.FormStore2.handleBlur(name, value);

  render() {
    return (
      <div className={`form__element ${this.props.FormStore2.fields[this.props.name].error ? 'form__element--error' : ''}`}>
        {this.props.htmlTag === 'input' &&
          <Input
            {...this.props}
            handleChange={this.handleChange}
            handleBlur={this.handleBlur}
          />
        }
        {this.props.htmlTag === 'select' &&
          <Select
            {...this.props}
            handleChange={this.handleChange}
            handleBlur={this.handleBlur}
          />
        }
        {this.props.htmlTag === 'textarea' &&
          <Textarea
            {...this.props}
            handleChange={this.handleChange}
            handleBlur={this.handleBlur}
          />
        }
        {this.props.FormStore2.fields[this.props.name].error &&
          <span
            className={'form__error'}
          >
            {this.props.errorMessage}
          </span>
        }
      </div>
    )
  }
}

FormElement.propTypes = {

};

FormElement.defaultProps = {

};

export default FormElement;
