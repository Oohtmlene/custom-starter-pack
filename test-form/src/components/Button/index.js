import React from 'react';
import { func, string, oneOf } from 'prop-types';

class Button extends React.Component {
  static propTypes = {
    type: oneOf(['primary', 'secondary', 'tertiary', 'inactive', 'switch']).isRequired,
    className: string,
    text: string,
    href: string,
    target: string,
    rel: string,
    name: string,
    onClick: func,
    data: string,
  };

  static defaultProps = {
    className: 'default',
    text: 'Send',
    href: '#',
    target: '_self',
    rel: '',
    style: {},
    name: '',
    data: '',
    onClick: () => { },
  };

  render() {
    const className = `button button--${this.props.type} button--${this.props.className}`;
    return (
      <a
        className={className}
        name={this.props.name}
        href={this.props.href}
        title={this.props.text}
        target={this.props.target}
        rel={this.props.rel}
        onClick={this.props.onClick}
        data-custom={this.props.data}
      >
        {this.props.text}
      </a>
    );
  }
}

export default Button;
