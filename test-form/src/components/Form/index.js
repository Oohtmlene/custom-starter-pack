import React, { Component } from 'react';
import {
  arrayOf,
  bool,
  func,
  object,
  objectOf,
  number,
  string,
  shape,
} from 'prop-types';
import { inject, observer } from 'mobx-react';
import Submit from './components/Submit';
import Fieldset from './components/Fieldset';
import PrevNext from '../PrevNext';

/*
  use mobx to feed this?
  formStore
  @observable fields = {
    firstname: '',
    name: '',
    address: '',
  }
  check mobx doc, error state etc should be manage there
  https://mobx.js.org/intro/concepts.html

  function to add the fields in the localstorage

  Then on components :
  pca predict: on populate: update the store
  onChange/blur: update the store
  Local storage: update the store
  value: "this.props.formStore"?

  So the store become the single source of truth.

*/
@inject('FormStore')
@observer
class Form extends Component {

  static propTypes = { // To complete
    form: shape({
      steps: bool,
      fieldset: arrayOf(shape({
        legend: string,
        fields: arrayOf(shape({
          row: number,
          col: number,
          formElement: string,
          type: string,
          name: string,
          id: string,
          label: string,
          required: bool,
          rules: string,
          placeholder: string,
          errorMessage: string,
          value: string,
          additionalInfo: string,
        })),
        additionalInfo: string,
      })),
    }),
    FormStore: shape({
      handleChange: func,
    }).isRequired,
  }

  static defaultProps = {
    form: {
      steps: false,
      // fields: {
      //   type: 'input',
      //   label: '',
      //   rules: 'optional',
      //   valid: false,
      //   value: '',
      // },
    },
  }


  //= Form must be my state holder? Or Form Elements?
  constructor() {
    super();
    this.state = { // will be updated by mobx
      error: false,
      step: 0,
    }
  }

  /*
  Componenent will mount:
  Add field to the store?
  And then easier to just call the store?
  What state can change:
  - value
  - valid
  */

  componentWillMount() {
    // Add fields to the store

    const Storefields = {}
    this.props.form.fieldsets.map((fieldset) => {
      fieldset.fields.map((field) => {
        //= handle excption when checkbox
        if (field.type === 'checkbox') {
          field.options.map((option) => {
            Storefields[option.name] = {
              value: option.value,
              error: false,
              rules: field.rules,
            };
            return option;
          });
          //= Other form elements
        } else {
          Storefields[field.name] = {
            value: field.value,
            error: false,
            rules: field.rules,
          };
        }
        return field;
      });
      return fieldset;
    });
    this.props.FormStore.fields = Storefields;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    /*
      1- check if field valid
        if valid => fetch
        if not show invalid field
    */

    const valid = this.props.FormStore.validateFields();

    if (valid) {
      //= Collect stored data (name/value pair)
      const data = {};
      const Storefields = this.props.FormStore.fields;
      Object.keys(Storefields).forEach((name) => {
        data[name] = Storefields[name].value
      });
      console.log('101 form', data);

      //= handle fetch
    }
  }

  //= Handle events
  handleChange = (name, value) => this.props.FormStore.handleChange(name, value);
  handleBlur = (name, value) => this.props.FormStore.handleBlur(name, value);
  handlePrev = () => this.setState({ step: this.state.step - 1 })
  handleNext = () => this.setState({ step: this.state.step + 1 })

  render() {
    console.log('139- form props', this.props);

    //= Fieldsets
    const fieldsetsList = this.props.form.fieldsets;
    const fieldsets = fieldsetsList.map((fieldset, i) =>
      <Fieldset
        {...fieldset}
        step={this.state.step}
        key={i.toString()}
        handleChange={this.handleChange}
        handleBlur={this.handleBlur}
      />
    );

    const showFieldsets = () => {
      const totalSteps = fieldsets.length;
      const currentStep = this.state.step;
      //= If form with steps
      if (this.props.form.steps) {
        return (
          <div className={`form__step${this.state.step}`}>
            <Fieldset
              { ...fieldsets[currentStep].props }
            />
            { /* 
              appear or not? Should be a component 
              component genre:
              prends 2 argument, currentStep, and number of step
              
              const PrevNext = (currentStep, totalSteps) => {
                <ul className="prev-next">
                  { currentStep > 0 &&
                    <Button text="previous step" onClick={this.handlePrev}/><br />
                  }
                  { currentStep < totalSteps && 
                   <Button text="next step" onClick={this.handleNext} />
                </ul>
              }

            
            */}
            {/* <Button text="previous step" onClick={this.handlePrev}/><br />
            <Button text="next step" onClick={this.handleNext} /> */}
            <PrevNext
              currentStep={currentStep}
              totalSteps={totalSteps}
              handlePrev={this.handlePrev}
              handleNext={this.handleNext}
            />
          </div>
        )
      } else {
        return (
          fieldsets
        )
      }
    }

    return (
      <form
        className={`form form--${this.props.className}`}
        id="form"
        action=""
      >
        <div className={'form__inner'}>
          {showFieldsets()}
        </div>
        <Submit
          onClick={this.handleSubmit} // if props handleSubmit handle submit else, this handle submit
        />
      </form>
    )
  };
}

export default Form;
