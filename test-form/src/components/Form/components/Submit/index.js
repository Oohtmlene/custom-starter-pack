import React from 'react';
import { bool, string, func } from 'prop-types';


const Submit = (props) => {
  return (
    <button
      className={`button button--submit`}
      type={props.buttonType}
      onClick={props.onClick}
      //disabled={props.disabled}
    >
      {props.name}
    </button>
  );
}

Submit.propTypes = {
  buttonType: string,
  name: string,
  className: string,
  onClick: func,
  disabled: bool,
};

Submit.defaultProps = {
  buttonType: 'primary',
  name: 'Submit the form',
  className: 'submit',
  disabled: true,
  onClick: () => { },
};

export default Submit;
