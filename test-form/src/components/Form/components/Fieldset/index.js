import React from 'react';
import { arrayOf, object, string } from 'prop-types';
import FormField from './components/FormField';
import './fieldset.css';

const Fieldset = (props) => {
  //= Layout: group per row
  const groupBy = (array, key) => {
    const newGroups = [];
    return array.reduce((groups, item) => {
      const val = item[key];
      newGroups[val] = groups[val] || [];
      newGroups[val].push(item);
      return newGroups;
    }, {});
  };
  const rows = groupBy(props.fields, 'row');
  console.log('rows', rows);
  const fields = rows.map((row, i) => (
    <div key={i.toString()} className="form__row"> {console.log('row', row, i)}
      {row.map(field =>
        (<FormField
          {...field}
          key={field.id}
          handleChange={props.handleChange}
          handleBlur={props.handleBlur}
        />))}
    </div>));
  return (
    <fieldset className="form__fieldset">
      <legend className="form__fieldset__legend">{props.legend}</legend>
      {props.intro &&
        <p className="form__fieldset__intro">{props.intro}</p>
      }
      {fields}
      {props.additionalInfo &&
        <p className="form__fieldset__additionalInfo">{props.additionalInfo}</p>
      }
    </fieldset>
  );
};

Fieldset.propTypes = {
  fields: arrayOf(object),
  legend: string,
  intro: string,
  additionalInfo: string,
};

Fieldset.defaultProps = {
  fields: [],
  legend: '',
  intro: '',
  additionalInfo: '',
};

export default Fieldset;
