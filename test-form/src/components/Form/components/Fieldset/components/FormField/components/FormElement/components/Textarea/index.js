import React from 'react';
import { arrayOf, bool, func, shape, string } from 'prop-types';

const Textarea = (props) => {
  const handleChange = (e) => {
    const name = props.name;
    const value = e.target.value;
    props.handleChange(name, value);
  }

  const handleBlur = (e) => {
    const name = props.name;
    const value = e.target.value;
    props.handleBlur(name, value);
  }

  return (
    <textarea
      id={`rf-${props.name}`}
      name={props.name}
      placeholder={props.placeholder}
      value={props.value}
      onChange={handleChange}
      onBlur={handleBlur}
    />
  );
}

Textarea.propTypes = {
};

Textarea.defaultProps = {
};

export default Textarea;

