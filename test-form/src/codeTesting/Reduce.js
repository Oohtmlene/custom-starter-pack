import React, { Component } from 'react';

class Reduce extends Component {

  render() {

    const colors = ['red', 'green', 'blue', 'red', 'black', 'purple', 'black']
    const mapColor = colors.map((color) => (<li key={color.toString + Math.random()}>{color}</li>));
    //= Syntax: arr.reduce(callback[, initialValue])
    //- Callback: 
    // Accumulator = accumulate the callback return value. t is the accumulated value previously returned in the last invocation of the callback, or initialValue, if supplied.
    // Current Value = Element being proceed in the array.
    // Current Index = The index of the current element being processed in the array. Starts at index 0, if an initialValue is provided, and at index 1 otherwise.
    // Array = The array reduce was called upon.
    const reduceColor = colors.reduce((accumulator, currentValue, currentIndex, array) => {
      console.log("Accumulator", accumulator); // return green, green
      console.log("Current Value", currentValue);
      console.log("Current Index", currentIndex);
      console.log("Array", array);
      console.log(accumulator);
      return (
        (accumulator.indexOf(currentValue) !== -1)
          ?
          accumulator :
          [...accumulator, currentValue],
          []
      );

    });

    console.log("reduce color", reduceColor);

    const distinctColors = colors.reduce((distinct, color) =>
      (distinct.indexOf(color) !== -1) ? distinct : [...distinct, color],
      []
    )
    console.log("distinct color", distinctColors);

    return (
      <div className="list">
        <h3>Testing</h3>
        {/* <ul>{mapColor}</ul>
        <h3>Reduced list</h3> */}
        {/* <ul>{reduceColor}</ul> */}
      </div>
    );
  }
}

export default Reduce;
