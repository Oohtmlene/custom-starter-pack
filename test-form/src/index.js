import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'mobx-react';

import registerServiceWorker from './registerServiceWorker';
//import Store from './stores/';
import Stores from './stores';
import TestForm from './scenes/TestForm';
import TestForm2 from './scenes/TestForm2';
import TestForm3 from './scenes/TestForm3';
import GoogleForm from './scenes/GoogleForm';
import Error from './scenes/Error';

render(
  <Provider {...Stores}>
    <Router>
      <Switch>
        <Route path="/google-form/" component={GoogleForm} />
        <Route path="/TestForm/" component={TestForm} />
        <Route path="/TestForm2/" component={TestForm2} />
        <Route path="/TestForm3/" component={TestForm3} />
        <Route component={Error} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
