import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Reduce from './codeTesting/Reduce';

class App extends Component {
  
  render() {

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div className="content">
          <section>
            <h2>Reduce</h2>
            <Reduce />
          </section>
        </div>
      </div>
    );
  }
}

export default App;
