import { observable, action } from 'mobx';
import Validation from '../helpers/Validation';

class FormStore2 {
  // The fields must match the form
  @observable fields = {
    firstname: {
      value: '',
      error: false,
      rules: 'required',
    },
    lastname: {
      value: '',
      error: false,
      rules: 'required',
    },
    phone: {
      value: '',
      error: false,
      rules: 'required|phone',
    },
    email: {
      value: '',
      error: false,
      rules: 'required|email',
    },
    options: {
      value: '',
      error: false,
      rules: 'required',
    },
  }
  @observable valid = false;

  // On input change check if valid and update field in store
  handleChange = (name, value) => {
    this.fields[name].value = value;
    //= When start typing again
    this.fields[name].error = false;
    // put error handling as well, just if match validation => error false, but not else.
  }

  // On input blur check if valid
  handleBlur = (name, value) => {
    /* value should match validation, if not cause error */
    //this.fields[name].error = true;
    // Check if field valid
    this.isFieldValid(name, value);
  }

  // is field valid function
  isFieldValid = (name, value) => {
    let valid = false;
    valid = Validation.isValid(value, this.fields[name].rules);
    this.fields[name].error = !valid; //= should I use @computed value for this kind of thing?
    return valid;
  }

  // is form valid function
  isFormValid = () => {
    let valid = true;
    //= For each field, check if !valid
    Object.keys(this.fields).forEach((name) => {
      if (!this.isFieldValid(name, this.fields[name].value)) {
        valid = false;
      }
    })
    this.valid = valid;
    return valid;
  }
}

export default new FormStore2();
