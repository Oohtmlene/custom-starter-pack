import { observable, action, computed } from 'mobx';
import Validation from '../helpers/Validation';

// to move logic and state out of your components
/*
  should contain error handling, validation, step steps etc
  good way to handle tabindex?
*/
// class FormStore {
//   @observable fields = {
//       firstname: {
//         value: 'helene',
//         error: false,
//         valid: false,
//       },
//       surname: {
//         value: 'ma',
//         error: false,
//         valid: false,
//       },
//     }
//     // Could it be just
//     /*  firstname: '',
//         surname: ''
//         basically all the fields in the app?

//     */

// }
// make it simple for now
class FormStore {
  @observable fields = { // to refactor, surely there is a way to make this automaticcally, but in a way if I put all my field in there, I have all the input asked to the client in the all app, which can be nice. but error and rules should be dynamic
    // title: {
    //   value: '',
    //   error: false,
    //   rules: 'required',
    // },
    // firstname: {
    //   value: '',
    //   error: false,
    //   rules: 'required',
    // },
    // location: {
    //   error: false,
    //   rules: 'required',
    //   value: 'in town, rural, etc',
    //
    // },
  }
  @observable valid = false;

  @computed get inUsedFields() {
    console.log('53- this store', this);
  }

  // Update on input change
  handleChange = (name, value) => {
    console.log('54 name', name);
    this.fields[name].value = value;
    //= When start typing again
    this.fields[name].error = false;
    // put error handling as well, just if match validation => error false, but not else.
  }

  // Update on input blur
  handleBlur = (name, value) => {
    /* value should match validation, if not cause error */
    //this.fields[name].error = true;
    console.log('65 name value', name, value);
    this.handleValidate(name, value);
  }

  // Validate function
  handleValidate = (name, value) => {
    let valid = false;
    valid = Validation.isValid(value, this.fields[name].rules);
    this.fields[name].error = !valid; //= should I use @computed value for this kind of thing?
    return valid;
  }

  // handle submit?
  validateFields = () => {
    let valid = true;
    //= For each field, check if !valid
    Object.keys(this.fields).forEach((name) => {
      console.log(name);
      if (!this.handleValidate(name, this.fields[name].value)) {
        valid = false;
      }
    })
    this.valid = valid;
    return valid;
  }
}

export default new FormStore();
