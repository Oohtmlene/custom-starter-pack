import React, { Component } from 'react';
import { bool, func, number, objectOf, shape, string } from 'prop-types';
import { inject, observer } from 'mobx-react';
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';
import Steps from '../../components/Steps';

@inject('FormStore2')
@observer
class TestForm2 extends Component {
  static propTypes = {
    FormStore2: objectOf(shape({})).isRequired,
  }

  render() {
    const storedFields = this.props.FormStore2.fields;
    return (
      <div>
        <h1>Form component option 2</h1>
        <Form
          steps="true"
          className="free-valuation"
        >
          <fieldset>
            <legend>Contact details</legend>
            <Field
              htmlTag="input"
              type="text"
              label="Firsname"
              name="firstname"
              placeholder="firsname"
              errorMessage="Please enter your firstname"
              value={storedFields.firstname ? storedFields.firstname.value : ''}
            />
            <Field
              htmlTag="input"
              type="text"
              label="Lastname"
              name="lastname"
              placeholder="lastname"
              errorMessage="Please enter your last name"
              value={storedFields.lastname ? storedFields.lastname.value : ''}
            />
            <h2>I can add whatever I want</h2>
            <Field
              htmlTag="input"
              type="phone"
              label="Phone"
              name="phone"
              placeholder="Phone Number"
              errorMessage="Must be a valid phone number"
              value={storedFields.phone ? storedFields.phone.value : ''}
            />
            <Field
              htmlTag="input"
              type="email"
              label="Email"
              name="email"
              placeholder="Enter your Email"
              errorMessage="Must be a valid email"
              value={storedFields.email ? storedFields.email.value : ''}
            />
            <Field
              htmlTag="input"
              type="radio"
              name="options"
              label="Select an option"
              options={[{
                name: 'option_1',
                value: 'Option 1',
              }, {
                name: 'option_2',
                value: 'Option 2',
              }, {
                name: 'option_3',
                value: 'Option 3',
              }]}
              rules="required"
              value={storedFields.title ? storedFields.title.value : ''}
            />
          </fieldset>
          <fieldset>
            <legend>By step layout</legend>
            <Steps
              total={3}
            >
              <div
                className="step1"
                nextText="Contact details"
              >
                <h3>Steps 1</h3>
              </div>
              <div className="step2">
                <h3>Steps 2</h3>
              </div>
              <div className="step3">
                <h3>Steps 3</h3>
              </div>
            </Steps>
          </fieldset>
        </Form>
      </div>
    );
  }
}

export default TestForm2;


/*

    static html, devide component

    <form class="form">
        //== step 1
        <fielset class="form__fieldset">
            <legend class="form__fieldset__legend">Contact details</legend>
            <div class="form__row">
                <div class="form__field form__field--firstname">
                    <label class="form__label form__label--firstname" for="surname">First name</label>
                    <input class="form__element form__element--firstname type="text" name="firstname" id="firstname" value="" placeholder="">
                </div>
                <div class="form__field form__field--surname">
                    <label class="form__label form__label--surname" for="surname">Surname</label>
                    <input class="form__element form__element--surname type="text" name="surname" id="surname" value="" placeholder="">
                </div>
            </div>
        </fielset>

        //== step 2
    </form>

    - if steps layout https://github.com/tommymarshall/react-multi-step-form/blob/master/src/javascript/components/Registration.jsx
    <Form class="form">
        case 1 => return step 1 can be whatever
        case 2 => return step 2 can be whatever
        case 3 => return step 3 can be whatever
    </form>

    - if no step layout
    <Form class="form">
        return step 1 can be whatever
        return step 2 can be whatever
        return step 3 can be whatever
    </form>

    Components

    Form
        How many versions?
        Step,
        No Step

        
        

*/


/*
    Each step a component - so I can reuse it?

    Address details
    Contact details
    Account details
    Additional details

    Should be allowd to be used together, separated, can add single field to them.
    Can have fieldset, no fieldsets, legend no legend, must include row
    Making it complete and flexible
    Should be able to receive data dynamically
    Allowing step layout or not
    1, 2, 3, 4 columns

    Add addditional info, p etc etc

    I need a pca predict component

    I need to make the local storage work as well, 
    be carefull on input change than the local storage does not override the value typed by the user.

    https://www2-staging.tepilo.com/request_callback/
    https://www2-staging.tepilo.com/valuation-visit/

*/

/*

Complete form Form

FormElement:
  input
    attr:
      type: 
        text
        radio
        submit
        checkbox
        file
        hidden
        image
        password
        button
        reset
        html5:
        date
        datetime-local
        email
        tel
        week
        month
        number
        range
        search
        time
        url
      name
      value
      readonly
      disabled
      size (in character)
      maxlength
    html5 attr:
      autocomplete
      autofocus
      form (input type submit)
      formaction (input type submit)
      formenctype (input type submit)
      formmethod
      formnovalidate
      formtarget
      height and width
      list //https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_datalist
      min and max
      multiple
      pattern
      placeholder
      required
      step
  select
     <select name="cars">
      <option value="volvo">Volvo</option>
      <option value="saab">Saab</option>
      <option value="fiat">Fiat</option>
      <option value="audi">Audi</option>
    </select> 
    attr:
      on <select>
        name
        size
        multiple
      on <option>
        value
    <textarea>
      name
      message
      row
      cols
  button
    type
    onclick
  
html5
  datalist 
    specifies a list of pre-defined options for an <input> element. //https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_datalist
    <datalist id="browsers"> 
      <option value="Internet Explorer">
      <option value="Firefox">
      <option value="Chrome">
      <option value="Opera">
      <option value="Safari">
    </datalist>
  output 
    result of a calculation 

  fieldset
    <fieldset> // attribut available: disabled, form, name
      <legend>Personalia:</legend>
      <input></input>
    </fieldset>
    
    ! important !
    <fieldset name form="form_id"> not supported for now but can be very handy

*/


/*
  Option 2
  Form component =>
    <form classname="form form--{props.className}">
        ${props.children}
    </form>
  Example of use:
  <Form className="free-valuation">
    <p>More flexible but how do I treat the data? </p>
    <Fieldset title="contact-details">
      <Select options={[option1, option2, option3]}/>
      <InputText name="firstname" value="" />
      <InputText name="surname" value="" />
    </Fiedset>
  </Form>

*/