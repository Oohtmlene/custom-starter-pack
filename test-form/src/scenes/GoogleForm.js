import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Form from '../components/Form'
import 'whatwg-fetch';

class GoogleForm extends Component {
    constructor() {
        super();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log('e', e.target);
        //const data = {};
        const data = 'form_field_1=test1&form_field_2=test2&form_field_3=test3&form_field_4=test'

        this.fetchGoogleApps('https://script.google.com/a/tepilo.com/macros/s/AKfycbyS5fHkdQrDndy1eJKIurFybdnWKCtepFVBkiK57CRlsju-LMA/exec', data);
    }

    fetchGoogleApps(url, data) {
        console.log('38 data', data);
        console.log('39 stringify data', JSON.stringify(data));
        fetch(url + '?' + data, {
            //credentials: 'include',
            method: 'get',
            dataType: "json",
            //data: JSON.stringify(data),
        })
            .then((response) => {
                console.log('response', response);
            })
            .catch((error) => {
                console.log('error');
            });
    }

    render() {
        const fields =
            [
                {
                    formElement: 'input',
                    type: 'text',
                    className: 'firstname',
                    name: 'first_name',
                    label: 'Firstname',
                    rules: 'required',
                    required: true,
                    placeholder: 'Please enter your firstname',
                    errorMessage: 'Must be a valid firstname',
                    value: 'Default value here',
                },
                {
                    formElement: 'input',
                    type: 'text',
                    className: 'surname',
                    name: 'surname',
                    label: 'Surname',
                    rules: 'required',
                    required: true,
                    placeholder: 'Please enter your surname',
                    errorMessage: 'Must be a valid surname',
                    value: '',
                },
            ];
        console.log('51', this.props);
        return (
            <Form
                className='google-form'
                fields={fields}
            />
        )
    }
}

export default GoogleForm;