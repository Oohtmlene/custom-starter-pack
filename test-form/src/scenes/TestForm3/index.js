import React, { Component } from 'react';
import { bool, func, number, objectOf, shape, string } from 'prop-types';
import { Form, Button, Checkbox, Message } from 'semantic-ui-react';

class TestForm3 extends Component {
  constructor() {
    super();
    this.state = {
      error: false,
    };
  }
  render() {
    const options = [
      { key: 'm', text: 'Male', value: 'male' },
      { key: 'f', text: 'Female', value: 'female' },
    ]
    return (
      <Form>
        Can I put anything anywhere?
        <Form.Group widths='equal'>
          <p>Like here for example</p>
          <Form.Input label='First name' placeholder='First name' error={this.state.error} />
          <Message error>Error on Firstname</Message>
          <Form.Input label='Last name' placeholder='Last name' />
        </Form.Group>
        <Form.Select options={options} placeholder='Gender' error />
        <Form.Checkbox label='I agree to the Terms and Conditions' error />
        <Form.Field>
          <label>First Name</label>
          <input placeholder='First Name' />
        </Form.Field>
        <Form.Field>
          <label>Last Name</label>
          <input placeholder='Last Name' />
        </Form.Field>
        <Form.Field>
          <Checkbox label='I agree to the Terms and Conditions' />
        </Form.Field>
        <Button type='submit'>Submit</Button>
      </Form>
    );
  }
}

export default TestForm3;


/*

    static html, devide component

    <form class="form">
        //== step 1
        <fielset class="form__fieldset">
            <legend class="form__fieldset__legend">Contact details</legend>
            <div class="form__row">
                <div class="form__field form__field--firstname">
                    <label class="form__label form__label--firstname" for="surname">First name</label>
                    <input class="form__element form__element--firstname type="text" name="firstname" id="firstname" value="" placeholder="">
                </div>
                <div class="form__field form__field--surname">
                    <label class="form__label form__label--surname" for="surname">Surname</label>
                    <input class="form__element form__element--surname type="text" name="surname" id="surname" value="" placeholder="">
                </div>
            </div>
        </fielset>

        //== step 2
    </form>

    - if steps layout https://github.com/tommymarshall/react-multi-step-form/blob/master/src/javascript/components/Registration.jsx
    <Form class="form">
        case 1 => return step 1 can be whatever
        case 2 => return step 2 can be whatever
        case 3 => return step 3 can be whatever
    </form>

    - if no step layout
    <Form class="form">
        return step 1 can be whatever
        return step 2 can be whatever
        return step 3 can be whatever
    </form>

    Components

    Form
        How many versions?
        Step,
        No Step

        
        

*/


/*
    Each step a component - so I can reuse it?

    Address details
    Contact details
    Account details
    Additional details

    Should be allowd to be used together, separated, can add single field to them.
    Can have fieldset, no fieldsets, legend no legend, must include row
    Making it complete and flexible
    Should be able to receive data dynamically
    Allowing step layout or not
    1, 2, 3, 4 columns

    Add addditional info, p etc etc

    I need a pca predict component

    I need to make the local storage work as well, 
    be carefull on input change than the local storage does not override the value typed by the user.

    https://www2-staging.tepilo.com/request_callback/
    https://www2-staging.tepilo.com/valuation-visit/

*/

/*

Complete form Form

FormElement:
  input
    attr:
      type: 
        text
        radio
        submit
        checkbox
        file
        hidden
        image
        password
        button
        reset
        html5:
        date
        datetime-local
        email
        tel
        week
        month
        number
        range
        search
        time
        url
      name
      value
      readonly
      disabled
      size (in character)
      maxlength
    html5 attr:
      autocomplete
      autofocus
      form (input type submit)
      formaction (input type submit)
      formenctype (input type submit)
      formmethod
      formnovalidate
      formtarget
      height and width
      list //https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_datalist
      min and max
      multiple
      pattern
      placeholder
      required
      step
  select
     <select name="cars">
      <option value="volvo">Volvo</option>
      <option value="saab">Saab</option>
      <option value="fiat">Fiat</option>
      <option value="audi">Audi</option>
    </select> 
    attr:
      on <select>
        name
        size
        multiple
      on <option>
        value
    <textarea>
      name
      message
      row
      cols
  button
    type
    onclick
  
html5
  datalist 
    specifies a list of pre-defined options for an <input> element. //https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_datalist
    <datalist id="browsers"> 
      <option value="Internet Explorer">
      <option value="Firefox">
      <option value="Chrome">
      <option value="Opera">
      <option value="Safari">
    </datalist>
  output 
    result of a calculation 

  fieldset
    <fieldset> // attribut available: disabled, form, name
      <legend>Personalia:</legend>
      <input></input>
    </fieldset>
    
    ! important !
    <fieldset name form="form_id"> not supported for now but can be very handy

*/


/*
  Option 2
  Form component =>
    <form classname="form form--{props.className}">
        ${props.children}
    </form>
  Example of use:
  <Form className="free-valuation">
    <p>More flexible but how do I treat the data? </p>
    <Fieldset title="contact-details">
      <Select options={[option1, option2, option3]}/>
      <InputText name="firstname" value="" />
      <InputText name="surname" value="" />
    </Fiedset>
  </Form>

*/