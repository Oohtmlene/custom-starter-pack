import React, { Component } from 'react';
import { bool, func, number, objectOf, shape, string } from 'prop-types';
import { inject, observer } from 'mobx-react';
import Form from '../../components/Form';

@inject('FormStore')
@observer
class TestForm extends Component {
  // static propTypes = {
  //   FormStore: shape({
  //     handleChange: func,
  //   }).isRequired,
  // }

  render() {
    const storedFields = this.props.FormStore.fields;
    console.log('17', this.props);
    const form = {
      steps: false,
      column: 4,
      // Fieldset arrays of fields, or any html, info, intro etc
      fieldsets: [ // Array of object
        // fieldset 1
        {
          legend: 'Address details',
          fields: [
            {
              row: 1,
              col: 2,
              formElement: 'input',
              type: 'text',
              name: 'line1',
              id: 'line1',
              label: 'Address line 1',
              required: true,
              rules: 'required',
              placeholder: 'Please enter your address',
              errorMessage: 'Please enter your address',
              value: storedFields.line1 ? storedFields.line1.value : '',
              additionalInfo: '',
            },
            {
              row: 1,
              col: 2,
              formElement: 'input',
              type: 'text',
              name: 'line2',
              id: 'line2',
              label: 'Address line 2',
              required: true,
              rules: 'required',
              placeholder: 'Please enter your address',
              errorMessage: 'Please enter your address',
              value: storedFields.line2 ? storedFields.line2.value : '',
              additionalInfo: '',
            },
            {
              row: 2,
              col: 2,
              formElement: 'input',
              type: 'radio',
              label: 'Gender',
              name: 'gender',
              required: true,
              rules: 'required',
              errorMessage: 'Please select your gender',
              options: [
                {
                  name: 'male',
                  value: 'Male',
                  //checked: true,
                },
                {
                  name: 'female',
                  value: 'Female',
                },
              ],
              value: storedFields.gender ? storedFields.gender.value : '',
              additionalInfo: '',
            },
          ],
          additionalInfo: '',
          submit: 'would be a fonction',
        },
        // fieldset 2
        {
          legend: 'Contact details',
          intro: 'Please enter your contact detail, blahdibldsfsdfah',
          fields: [
            {
              row: 1,
              col: 1,
              formElement: 'select',
              options: [
                {
                  value: 'Mr',
                  text: 'Mr',
                },
                {
                  value: 'Mrs',
                  text: 'Mrs',
                },
                {
                  value: 'Miss',
                  text: 'Miss',
                },
              ],
              name: 'title',
              id: 'title',
              label: 'Title',
              required: true,
              rules: 'required',
              placeholder: 'Please select a value',
              errorMessage: 'Please select a value',
              value: storedFields.title ? storedFields.title.value : '',
              additionalInfo: '',
            },
            {
              row: 2,
              col: 2,
              formElement: 'input',
              type: 'text',
              name: 'firstname',
              id: 'firstname',
              label: 'First name',
              required: true,
              rules: 'required',
              placeholder: 'Please enter your first name',
              errorMessage: 'Please enter your first name',
              value: storedFields.firstname ? storedFields.firstname.value : '',
              additionalInfo: '',
            },
            {
              row: 2,
              col: 2,
              formElement: 'input',
              type: 'text',
              name: 'surname',
              id: 'surname',
              label: 'Surname',
              required: true,
              rules: 'required',
              placeholder: 'Please enter your surname',
              errorMessage: 'Please enter your surname',
              value: storedFields.surname ? storedFields.surname.value : '',
              additionalInfo: '',
            },
          ],
          additionalInfo: "<p>Here I can put additional info, + <a>link?</a> (T & C's) use dangeroulsy html</p>",
          submit: 'would be a fonction, or just handle via form',
        },
        // fieldset 3
        {
          legend: 'Property details',
          fields: [
            {
              row: 1,
              col: 1,
              formElement: 'textarea',
              name: 'message',
              id: 'message',
              label: 'Message',
              required: false,
              rules: 'optional',
              placeholder: 'Message',
              value: storedFields.message ? storedFields.message.value : '',
              additionalInfo: '',
            },
            {
              formElement: 'input',
              type: 'checkbox',
              name: 'test',
              id: 'rural',
              label: 'Rural',
              options: [
                {
                  name: 'Rural',
                  value: true,
                },
                {
                  name: 'In Town',
                  value: true,
                },
              ],
              required: false,
              rules: 'optional',
              value: 'test',
              additionalInfo: '',
            },
            // {
            //   formElement: 'input',
            //   type: 'checkbox',
            //   name: 'rural',
            //   id: 'rural',
            //   label: 'Rural',
            //   required: false,
            //   rules: 'optional',
            //   value: storedFields.rural ? storedFields.rural.value : false,
            //   additionalInfo: '',
            // },
            // {
            //   formElement: 'input',
            //   type: 'checkbox',
            //   name: 'in_town',
            //   id: 'in-town',
            //   label: 'In town',
            //   required: false,
            //   rules: 'optional',
            //   value: storedFields.in_town ? storedFields.in_town.value : false,
            //   additionalInfo: '',
            // },
          ],
          additionalInfo: '',
        },
        // {
        //   legend: 'Additional details',
        //   // fieldset 4
        //   fields: [
        //     {
        //       formElement: 'input',
        //       type: 'checkbox',
        //       name: 'conditions',
        //       options: [
        //         {
        //           name: '',
        //           value: 'Please tick this box if you would like to receive further news and updates from Tepilo',
        //         },
        //       ],
        //       id: 't-and-cs',
        //       label: '',
        //       required: true,
        //       rules: 'required',

        //       value: storedFields.conditions ? storedFields.conditions.value : '',
        //       additionalInfo: '',
        //     },
        //   ],
        //   additionalInfo: '',
        //   submit: 'would be a fonction',
        // },
      ],
    };
    return (
      <div>
        <h1>Form component option 1</h1>
        <Form
          form={form}
          className="test-form"
        />
      </div>
    );
  }
}

export default TestForm;


/*

    static html, devide component

    <form class="form">
        //== step 1
        <fielset class="form__fieldset">
            <legend class="form__fieldset__legend">Contact details</legend>
            <div class="form__row">
                <div class="form__field form__field--firstname">
                    <label class="form__label form__label--firstname" for="surname">First name</label>
                    <input class="form__element form__element--firstname type="text" name="firstname" id="firstname" value="" placeholder="">
                </div>
                <div class="form__field form__field--surname">
                    <label class="form__label form__label--surname" for="surname">Surname</label>
                    <input class="form__element form__element--surname type="text" name="surname" id="surname" value="" placeholder="">
                </div>
            </div>
        </fielset>

        //== step 2
    </form>

    - if steps layout https://github.com/tommymarshall/react-multi-step-form/blob/master/src/javascript/components/Registration.jsx
    <Form class="form">
        case 1 => return step 1 can be whatever
        case 2 => return step 2 can be whatever
        case 3 => return step 3 can be whatever
    </form>

    - if no step layout
    <Form class="form">
        return step 1 can be whatever
        return step 2 can be whatever
        return step 3 can be whatever
    </form>

    Components

    Form
        How many versions?
        Step,
        No Step

        
        

*/


/*
    Each step a component - so I can reuse it?

    Address details
    Contact details
    Account details
    Additional details

    Should be allowd to be used together, separated, can add single field to them.
    Can have fieldset, no fieldsets, legend no legend, must include row
    Making it complete and flexible
    Should be able to receive data dynamically
    Allowing step layout or not
    1, 2, 3, 4 columns

    Add addditional info, p etc etc

    I need a pca predict component

    I need to make the local storage work as well, 
    be carefull on input change than the local storage does not override the value typed by the user.

    https://www2-staging.tepilo.com/request_callback/
    https://www2-staging.tepilo.com/valuation-visit/

*/

/*

Complete form Form

FormElement:
  input
    attr:
      type: 
        text
        radio
        submit
        checkbox
        file
        hidden
        image
        password
        button
        reset
        html5:
        date
        datetime-local
        email
        tel
        week
        month
        number
        range
        search
        time
        url
      name
      value
      readonly
      disabled
      size (in character)
      maxlength
    html5 attr:
      autocomplete
      autofocus
      form (input type submit)
      formaction (input type submit)
      formenctype (input type submit)
      formmethod
      formnovalidate
      formtarget
      height and width
      list //https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_datalist
      min and max
      multiple
      pattern
      placeholder
      required
      step
  select
     <select name="cars">
      <option value="volvo">Volvo</option>
      <option value="saab">Saab</option>
      <option value="fiat">Fiat</option>
      <option value="audi">Audi</option>
    </select> 
    attr:
      on <select>
        name
        size
        multiple
      on <option>
        value
    <textarea>
      name
      message
      row
      cols
  button
    type
    onclick
  
html5
  datalist 
    specifies a list of pre-defined options for an <input> element. //https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_datalist
    <datalist id="browsers"> 
      <option value="Internet Explorer">
      <option value="Firefox">
      <option value="Chrome">
      <option value="Opera">
      <option value="Safari">
    </datalist>
  output 
    result of a calculation 

  fieldset
    <fieldset> // attribut available: disabled, form, name
      <legend>Personalia:</legend>
      <input></input>
    </fieldset>
    
    ! important !
    <fieldset name form="form_id"> not supported for now but can be very handy

*/


/*
  Option 2
  Form component =>
    <form classname="form form--{props.className}">
        ${props.children}
    </form>
  Example of use:
  <Form className="free-valuation">
    <p>More flexible but how do I treat the data? </p>
    <Fieldset title="contact-details">
      <Select options={[option1, option2, option3]}/>
      <InputText name="firstname" value="" />
      <InputText name="surname" value="" />
    </Fiedset>
  </Form>

*/