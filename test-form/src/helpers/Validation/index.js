import validator from 'validator';

class Validation {
  rules = {
    phoneRegex: /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?#(\d{4}|\d{3}))?$/,
  };

  /**
   * check the rule is active or not
   * @param {string}
   * @return {boolean}
   */
  isRule = (value, rules) =>
    rules.split('|').indexOf(value) !== -1;

  /**
   * @return {boolean}
   */
  isValid = (val, rules) => {
    // Required validation
    if (this.isRule('required', rules) && val === '') {
      return false;
    }

    // Email validation
    if (this.isRule('email', rules) && !validator.isEmail(val)) {
      return false;
    }

    // Firstname validation
    if (this.isRule('firstname', rules) && (val.length < 2 || !validator.isAlpha(val))) {
      return false;
    }

    // Surname validation
    if (this.isRule('surname', rules) && (val.length < 2 || !validator.isAlpha(val))) {
      return false;
    }

    // Message validaiton
    if (this.isRule('message', rules) && val.length < 10) {
      return false;
    }

    // Phone number validation
    if (this.isRule('phone', rules) && !this.rules.phoneRegex.test(val)) {
      return false;
    }

    // New Password validation
    if (this.isRule('password', rules) && (val.length < 6)) {
      return false;
    }

    return true;
  };
}

export default new Validation();
