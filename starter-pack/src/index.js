import React from 'react';
import ReactDOM from 'react-dom';
import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import Home from './js/scenes/Home';
import MyProperties from './js/scenes/MyProperties';
import Error404 from './js/scenes/Error404';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Router>
        <Switch>
            <Route IndexRoute exact path="/my-dashboard" component={Home} />
            <Route path="/my-properties" component={MyProperties} />
            <Route component={Error404} />
        </Switch>
    </Router>,
    document.getElementById('root'),
);
registerServiceWorker();