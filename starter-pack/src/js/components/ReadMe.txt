One component => one single responsability
Components defined at the root level of your project, in the components folder, are global and can be used anywhere in the application.
If we define a new component inside another component, this new component can only be used by its direct parent.
/src
  /components
    /Button => Can be used globally
      /index.js

    /Notifications 
      /components 
        /ButtonDismiss => Can only be used within the Notifications components
          /index.js

      /actions.js
      /index.js
      /reducer.js


Styles, scss, variables, base etc etc see how to import