import React from 'react';
import { Nav } from '../../../../components'
import './styles/main-nav.css';

const MainNav = (props) => {
    console.log('mainnav', props);
    const items = [
        {
            name: "home",
            title: "Home",
            link: process.env.REACT_APP_URL_BASE,
            target: "",
            tabIndex: "0",
            type: "internal",

        },
        {
            name: "my-properties",
            title: "My Properties",
            link: process.env.REACT_APP_URL_MYPROPERTIES,
            target: "",
            tabIndex: "-1",
            type: "internal",
        },
        {
            name: "contact",
            title: "Contact",
            link: "#",
            target: "",
            tabIndex: "-2",
            type: "internal",
        },
    ]

    return (
        <Nav items={items}/>
    );

}

export default MainNav;