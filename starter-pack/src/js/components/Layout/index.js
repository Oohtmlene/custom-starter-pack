import React from 'react';
import { node, string } from 'prop-types';
import MainNav from './components/MainNav';
import './styles/layout.css';

const Layout = (props) => (
    <div className={`page page--${props.page}`}>
        <MainNav /> {console.log('Layout', props)}
        <h1>{props.title}</h1>
        {props.children}
    </div>
);

Layout.propTypes = {
    page: string.isRequired,
    title: string.isRequired,
    children: node.isRequired,
};


export default Layout;