import React, { Component } from 'react';
import { arrayOf, object, string } from 'prop-types';

class Tabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 0,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    const selected = e.target.tabIndex;
    this.setState({ selected });
  }

  render() {
    const {
      className,
      options,
    } = this.props;

    return (
      <div className={`tabs tabs--${className}`}>
        <div className="inner">
          <ul className="tabs__nav">
            {options.map((option, i) =>
              (
                <li className="tabs__nav__item" key={i.toString()}>
                  <a
                    className={`tabs__nav__item__link ${this.state.selected === i ? 'is-active' : 'is-inactive'}`}
                    role="MenuItem"
                    tabIndex={i}
                    onClick={this.handleClick}
                  >
                    {option.title}
                  </a>
                </li>
              ),
            )}
          </ul>
          <div className="tabs__content">
            {options.map((option, i) =>
              (this.state.selected === i ? <div className="tabs__content__item" key={i.toString()}> {option.content} </div> : ''),
            )}
          </div>
        </div>
      </div>
    );
  }
}

Tabs.propTypes = {
  className: string,
  options: arrayOf(object).isRequired,
};

Tabs.defaultProps = {
  className: 'default',
};

export default Tabs;
