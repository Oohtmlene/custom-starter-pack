import React from 'react';
import { bool, number } from 'prop-types';

const Step = ({ active = false, completed = false, num }) =>
  (<li className={`steps__item ${(active) ? 'is-active' : ''}${(completed) ? 'is-completed' : ''}`}>
    { num }
  </li>);

Step.propTypes = {
  active: bool.isRequired,
  completed: bool.isRequired,
  num: number.isRequired,
};

export default Step;
