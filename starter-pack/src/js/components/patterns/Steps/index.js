import React from 'react';
import { number, string } from 'prop-types';
import Step from './components/Step';

const Steps = ({ ...props }) =>
  (<ul className={`steps steps--${props.className}`} >
    {[...Array(props.total)].map((n, i) =>
      (
        <Step
          key={i.toString()}
          num={i + 1}
          active={props.step === (i + 1)}
          completed={props.step > (i + 1)}
        />
      ),
    )}
  </ul>);

Steps.propTypes = {
  total: number.isRequired,
  step: number.isRequired,
  className: string,
};

Steps.defaultProps = {
  total: 1,
  step: 0,
  className: 'default',
};

export default Steps;
