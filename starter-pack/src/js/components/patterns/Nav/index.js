
import React from 'react';
import { any, arrayOf, string } from 'prop-types';
import NavItem from './components/NavItem';

const Nav = (props) => (
  <nav className={`nav nav--${props.className}`}>
    {console.log('Nav', props)}
    <ul className="nav__items">
      { props.items.map((item) => (
          <NavItem
            key={item.name} 
            {...item}
          />
      ))}
    </ul>
  </nav>
);

Nav.propTypes = {
   className: string,
   item: arrayOf(any),
}

Nav.defaultProps = {
    className: 'default',
}

export default Nav;


/*
  <nav class="nav nav--default">
    <ul class="list list--default">
      <li class="list__item list__item--default"></li>
      <li class="list__item list__item--default"></li>
      <li class="list__item list__item--default"></li>
    </ul>
  </nav>
  
  <ul class="list list--default">
    <li class="list__item list__item--default"><a></a></li>
    <li class="list__item list__item--default"><a></a></li>
    <li class="list__item list__item--default"><a></a></li>
    <li class="list__item list__item--default"><a></a></li>
  </ul>
  <ul class="list list--default">
    <li class="list__item list__item--default"><span></span></li>
    <li class="list__item list__item--default"><span></span></li>
    <li class="list__item list__item--default"><span></span></li>
  </ul>

*/