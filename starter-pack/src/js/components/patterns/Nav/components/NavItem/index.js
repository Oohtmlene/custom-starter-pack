
import React from 'react';
import { oneOf } from 'prop-types';
import { NavLink } from 'react-router-dom';

const NavItem = (props) => {
  // Nav link can be external or internal
  console.log('NavItem', props)
  const link = props.type === 'internal'
    ?
    <NavLink to={props.link}>{props.title}</NavLink>
    : <a href={props.link}>{props.title}</a>;
  return (
    <li
      className={`nav__item nav__item--${props.name}`}
    >   {console.log('NavItem props.link', NavLink)}
      {link}
    </li>
  )
}

NavItem.propTypes = {
  type: oneOf(['internal', 'external']),
}

NavItem.defaultProps = {
  type: 'internal'
}

export default NavItem;


/*
  <nav class="nav nav--default">
    <ul class="list list--default">
      <li class="list__item list__item--default"></li>
      <li class="list__item list__item--default"></li>
      <li class="list__item list__item--default"></li>
    </ul>
  </nav>
  
  <ul class="list list--default">
    <li class="list__item list__item--default"><a></a></li>
    <li class="list__item list__item--default"><a></a></li>
    <li class="list__item list__item--default"><a></a></li>
    <li class="list__item list__item--default"><a></a></li>
  </ul>
  <ul class="list list--default">
    <li class="list__item list__item--default"><span></span></li>
    <li class="list__item list__item--default"><span></span></li>
    <li class="list__item list__item--default"><span></span></li>
  </ul>

*/