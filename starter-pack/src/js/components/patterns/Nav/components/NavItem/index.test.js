import React from 'react';
import ReactDOM from 'react-dom';
import NavItem from '../NavItem';
import { MemoryRouter } from 'react-router-dom'
import renderer from 'react-test-renderer';


//= Testing if it's rendering what I expect
it('renders correctly', () => {
  const tree = renderer.create(
    <MemoryRouter>
      <NavItem to="/my-dashboard">My dashboard</NavItem>
    </MemoryRouter>
  ).toJSON(); 
  expect(tree).toMatchSnapshot();
});




//= Discribe what Nav item must do
// Should render correctly
//= Should apply active class active when active route

// test('Link change class when active route', () => {
//   const component = renderer.create(
//     <NavItem page="http://www.facebook.com">Facebook</NavItem>
//   );
// });

/*
  <StaticRouter location="" context={context}>
      <NavItem to="/my-dashboard">My dashboard</NavItem>
    </StaticRouter>


*/