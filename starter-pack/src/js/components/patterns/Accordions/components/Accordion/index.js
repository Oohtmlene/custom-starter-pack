import React, { Component } from 'react';
import { number, string } from 'prop-types';


class Accordion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 0,
      collapsing: 'is-collapsed',
      open: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    const open = !this.state.open;
    this.setState({ open });
    return false;
  }

  render() {
    return (
      <li className={`accordion__item ${this.state.open ? 'is-open' : 'is-closed'}`}>
        <a
          className="accordion__item__link"
          role="MenuItem"
          tabIndex={this.props.index}
          onClick={this.handleClick}
        >
          {this.props.title}
        </a>
        <div className="accordion__item__content">
          {this.props.content}
        </div>
      </li>
    );
  }
}

Accordion.propTypes = {
  title: string.isRequired,
  content: string.isRequired,
  index: number.isRequired,
};

export default Accordion;
