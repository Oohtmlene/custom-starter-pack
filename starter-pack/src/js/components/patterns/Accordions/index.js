import React from 'react';
import { arrayOf, object, string } from 'prop-types';
import Accordion from './components/Accordion';

const Accordions = ({ ...props }) =>
  (
    <div className={`accordion accordion--${props.className}`}>
      <ul className="accordion__list inner">
        {props.options.map((option, i) =>
          (
            <Accordion
              index={i}
              title={option.title}
              content={option.content}
            />
          ),
        )}
      </ul>
    </div>
  );

Accordions.propTypes = {
  className: string,
  options: arrayOf(object).isRequired,
};

Accordions.defaultProps = {
  className: 'default',
};

export default Accordions;
