import React from 'react';
import ListItem from './ListItem';
import { } from 'prop-types';

const List = (props) => (
  <ul className={`list list--${props.className}`} >
    { props.items.map((item) => (
        <ListItem {...item} />
    ))}
  </ul>
);

List.propTypes = {
  
};

List.defaultProps = {
 
};

export default List;


/*
  ul className="list">
    {console.log('7 List', props)}

    <ListItem items={props.items} />
    // need to map the list
    text
  </ul>

*/