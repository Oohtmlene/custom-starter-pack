import React from 'react';
import { string } from 'prop-types';

const ListItem = (props) => {
  return (
    <li className={`list list--${props.classname}}`}>{props.children}</li>
  );
}

ListItem.propTypes = {
  className: string,
};

ListItem.defaultProps = {
  className: 'default',
};

export default ListItem;

/*

  this.props.children
  <ListItem className="">
    <a href="#">test</a>
  </ListItem>

*/