import React, { Component } from 'react';
import { } from 'prop-types';
import { Layout } from '../../components'
import './styles/home.css';

class Home extends Component {
  
  render() {
    {console.log('Home', this)}
    return (
      <Layout
        title='Homepage'
        page='home'
        {...this.props}
        >
        <p>This is the content of the home page</p> 
      </Layout>
    );
  }
}

export default Home;
