import React from 'react';
import { Layout } from '../../components'

const MyProperties = () =>
    <Layout
      title='My properties'
      page='my-properties'
      {...this.props}
      >
      <p>This is the content of the property page</p> 
    </Layout>


export default MyProperties;
