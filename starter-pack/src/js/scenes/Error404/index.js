import React from 'react';
import { } from 'prop-types';
import { Layout } from '../../components'

const Error404 = ( ) => 
    <Layout
        title='Error page'
        page='error'
    >
        <p>Error page, please go back</p> 
    </Layout>

Error404.propTypes = {};

export default Error404;